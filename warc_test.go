package warc

import (
	"github.com/gocolly/colly"
	"net/http"
	"net/url"
	"testing"
)

var (
	request  *colly.Request
	response *colly.Response
)

func init() {
	exURL, _ := url.Parse("https://example.com")
	headers := &http.Header{}
	headers.Set("User-Agent", "colly")

	request = &colly.Request{
		URL:     exURL,
		Method:  "GET",
		Headers: headers,
	}

	resHeaders := &http.Header{}
	headers.Set("Content-Type", "text/html")

	response = &colly.Response{
		Request:    request,
		StatusCode: 200,
		Headers:    resHeaders,
		Body:       []byte("<!DOCTYPE html>\r\n<html>\r\n<body>\r\nsome text\r\n</body>\r\n</html>"),
	}
}

func TestRequestRecordFromColly(t *testing.T) {
	_, err := requestRecordFromColly(request)
	if err != nil {
		t.Fatal(err)
	}
}

func TestResponseRecordFromColly(t *testing.T) {
	_, err := responseRecordFromColly(response)
	if err != nil {
		t.Fatal(err)
	}
}
