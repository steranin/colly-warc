# colly-warc
[![GoDoc](http://godoc.org/gitlab.com/steranin/colly-warc?status.svg)](http://godoc.org/gitlab.com/steranin/colly-warc)
[![pipeline status](https://gitlab.com/steranin/colly-warc/badges/master/pipeline.svg)](https://gitlab.com/steranin/colly-warc/commits/master)
[![coverage report](https://gitlab.com/steranin/colly-warc/badges/master/coverage.svg)](https://gitlab.com/steranin/colly-warc/commits/master)

colly-warc adds warc file writing support to [Colly](https://github.com/gocolly/colly).

# Usage
```go
func main() {

  warcWriter, _ := warc.WarcWriter("/path/to/warc.gz")
	c := colly.NewCollector(warcWriter)

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL)
	})

	c.Visit("https://example.com/")
}
```
