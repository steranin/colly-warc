package warc

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"github.com/datatogether/warc"
	"github.com/gabriel-vasile/mimetype"
	"github.com/gocolly/colly"
	"io"
	"os"
	"path"
	"time"
)

func WarcWriter(filename string) (func(c *colly.Collector), error) {
	f, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE, 0660)
	if err != nil {
		return nil, err
	}

	writer, err := NewCompressedWarcFileWriter(f)
	if err != nil {
		return nil, err
	}

	writer.WriteInfoRecord(map[string]string{"software": "colly"}, path.Base(filename))

	return func(c *colly.Collector) {
		defer f.Close()
		c.OnRequest(writer.WriteRequestRecord)
		c.OnResponse(writer.WriteResponseRecord)
	}, nil
}

type warcFileWriter struct {
	writer   *warc.Writer
	records  *warc.Records
	warcFile *os.File
}

func NewCompressedWarcFileWriter(f io.WriteSeeker) (*warcFileWriter, error) {
	compress := gzip.NewWriter(f)
	w, err := warc.NewWriterCompressed(f, compress)
	if err != nil {
		return nil, err
	}

	return &warcFileWriter{writer: w, records: nil}, err
}

func NewWarcFileWriter(f io.Writer) (*warcFileWriter, error) {
	w, err := warc.NewWriterRaw(f)
	if err != nil {
		return nil, err
	}

	return &warcFileWriter{writer: w, records: nil}, err
}

func (w *warcFileWriter) WriteInfoRecord(metadata map[string]string, filename string) error {
	warcInfoContent := new(bytes.Buffer)
	for k, v := range metadata {
		warcInfoContent.WriteString(fmt.Sprintf("%s: %s\r\n", k, v))
	}

	warcInfo := &warc.Record{
		Format: warc.RecordFormatWarc,
		Type:   warc.RecordTypeWarcInfo,
		Headers: map[string]string{
			warc.FieldNameWARCRecordID:  warc.NewUUID(),
			warc.FieldNameWARCType:      warc.RecordTypeWarcInfo.String(),
			warc.FieldNameWARCFilename:  filename,
			warc.FieldNameWARCDate:      time.Now().Format(time.RFC3339),
			warc.FieldNameContentType:   "application/warc-fields",
			warc.FieldNameContentLength: fmt.Sprintf("%d", len(warcInfoContent.Bytes())),
		},
		Content: warcInfoContent,
	}

	_, _, err := w.writer.WriteRecord(warcInfo)

	return err
}

func requestRecordFromColly(r *colly.Request) (*warc.Record, error) {
	content := new(bytes.Buffer)
	content.WriteString(r.Method)
	content.WriteString("\r\n")
	warc.WriteHTTPHeaders(content, *r.Headers)
	if r.Body != nil {
		body := new(bytes.Buffer)
		body.ReadFrom(r.Body)
		content.Write(body.Bytes())
	}

	return &warc.Record{
		Format: warc.RecordFormatWarc,
		Type:   warc.RecordTypeRequest,
		Headers: map[string]string{
			warc.FieldNameWARCType:          warc.RecordTypeRequest.String(),
			warc.FieldNameWARCRecordID:      warc.NewUUID(),
			warc.FieldNameWARCTargetURI:     r.URL.String(),
			warc.FieldNameWARCDate:          time.Now().Format(time.RFC3339),
			warc.FieldNameWARCPayloadDigest: warc.Sha1Digest(content.Bytes()),
			warc.FieldNameContentType:       "application/http; msgtype=request",
			warc.FieldNameContentLength:     fmt.Sprintf("%d", len(content.Bytes())),
		},
		Content: content,
	}, nil
}

func (w *warcFileWriter) WriteRequestRecord(r *colly.Request) {
	rec, _ := requestRecordFromColly(r)

	_, _, err := w.writer.WriteRecord(rec)
	if err != nil {
		return
	}
}

func responseRecordFromColly(r *colly.Response) (*warc.Record, error) {
	mime, _ := mimetype.Detect(r.Body)
	content := new(bytes.Buffer)
	warc.WriteHTTPHeaders(content, *r.Headers)

	sanitized, err := warc.Sanitize(mime, r.Body)
	if err != nil {
		return nil, err
	}
	content.Write(sanitized)

	return &warc.Record{
		Format: warc.RecordFormatWarc,
		Type:   warc.RecordTypeResponse,
		Headers: map[string]string{
			warc.FieldNameWARCPayloadDigest:         warc.Sha1Digest(r.Body),
			warc.FieldNameContentType:               "application/http; msgtype=response",
			warc.FieldNameWARCIdentifiedPayloadType: mime,
			warc.FieldNameWARCTargetURI:             r.Request.URL.String(),
			warc.FieldNameWARCType:                  warc.RecordTypeResponse.String(),
			warc.FieldNameWARCRecordID:              warc.NewUUID(),
			warc.FieldNameWARCDate:                  time.Now().Format(time.RFC3339),
			warc.FieldNameContentLength:             fmt.Sprintf("%d", len(content.Bytes())),
		},
		Content: content,
	}, nil
}

func (w *warcFileWriter) WriteResponseRecord(r *colly.Response) {
	rec, err := responseRecordFromColly(r)
	if err != nil {
		return
	}

	_, _, err = w.writer.WriteRecord(rec)
	if err != nil {
		return
	}
}
